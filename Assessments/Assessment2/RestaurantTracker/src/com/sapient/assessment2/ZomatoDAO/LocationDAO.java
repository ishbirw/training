package com.sapient.assessment2.ZomatoDAO;

import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.sapient.assessment2.POJO.LocationPOJO;
import com.sapient.assessment2.POJO.RestaurantPOJO;

public class LocationDAO {
	public static String API_KEY = "66bc81fe8b837a4e8e6695ba6f7a7ccc";
	
	public LocationDAO() {
	}
	
	private static URI getBaseURI()
	{
		return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/" + "").build();
	}
	
	public LocationPOJO searchLocation(String query)
	{
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseURI());
		
		String res = target.path("locations").queryParam("query", query).request().header("user-key",API_KEY).accept(MediaType.APPLICATION_JSON).get(String.class);
		
		Object obj;
		LocationPOJO location_pojo = null;
		try {
			obj = new JSONParser().parse(res);
			JSONObject jo = (JSONObject) obj;
			
			String status =(String)jo.get("status");
			if(status.equalsIgnoreCase("success"))
			{
				JSONArray ja = (JSONArray) jo.get("location_suggestions");
				Iterator itr1 = ja.iterator();
				
				while(itr1.hasNext())
				{
					Map location = (Map)itr1.next();
					String e_type = (String)location.get("entity_type");
					Long e_id = (Long)location.get("entity_id");
					double lat = (Double)location.get("latitude");
					double lon = (Double)location.get("longitude");
					String title = (String)location.get("title");
					
					location_pojo = new LocationPOJO(e_type,e_id,lat,lon,title);
					return location_pojo;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return location_pojo;		
	}
	
	
	
	public List<RestaurantPOJO> searchLocationDetails(LocationPOJO query)
	{
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseURI());
		
		String res = target.path("location_details").queryParam("entity_id",query.getEntity_id()).queryParam("entity_type", query.getEntity_type()).request().header("user-key",API_KEY).accept(MediaType.APPLICATION_JSON).get(String.class);
		
		Object obj;
		List<RestaurantPOJO> restaurant_list = new ArrayList<RestaurantPOJO>();
		
		try {
			
			obj = new JSONParser().parse(res);
			JSONObject jo = (JSONObject) obj;
			
			JSONArray ja = (JSONArray) jo.get("best_rated_restaurant");
			Iterator itr1 = ja.iterator();
			
			while(itr1.hasNext())
			{
				Map temp = (Map)itr1.next();
				Map restaurant = (Map)temp.get("restaurant");
				
				String id = (String)restaurant.get("id");
				String name = (String)restaurant.get("name");
				String url = (String)restaurant.get("url");
				String address = (String)((Map)restaurant.get("location")).get("address");
				String cuisine = (String)restaurant.get("cuisines");
				
				RestaurantPOJO temp_restaurant = new RestaurantPOJO(id,name,url,address,cuisine);
				restaurant_list.add(temp_restaurant);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return restaurant_list;		
	}
	
	
	
	
}
