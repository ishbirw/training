package com.sapient.assessment2.POJO;

public class LocationPOJO {
	
	private String entity_type;
	private Long entity_id;
	private double lat;
	private double lon;
	private String title;
	
	public LocationPOJO() {
		super();
	}
	
	public LocationPOJO(String entity_type, Long e_id, double lat, double lon, String title) {
		super();
		this.entity_type = entity_type;
		this.entity_id = e_id;
		this.lat = lat;
		this.lon = lon;
		this.title = title;
	}


	@Override
	public String toString() {
		return "LocationPOJO [entity_type=" + entity_type + ", entity_id=" + entity_id + ", lat=" + lat + ", lon=" + lon
				+ ", title=" + title + "]";
	}

	public String getEntity_type() {
		return entity_type;
	}

	public void setEntity_type(String entity_type) {
		this.entity_type = entity_type;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.entity_type = title;
	}
	

	public Long getEntity_id() {
		return entity_id;
	}

	public void setEntity_id(Long entity_id) {
		this.entity_id = entity_id;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}
	
	
}
