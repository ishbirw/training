package com.sapient.assessment2.POJO;

public class RestaurantPOJO {
	
	private String id;
	private String name;
	private String url;
	private String address;
	private String cuisines;
	
	

	public RestaurantPOJO(String id, String name, String url, String address, String cuisines) {
		super();
		this.id = id;
		this.name = name;
		this.url = url;
		this.address = address;
		this.cuisines = cuisines;
	}
	
	public RestaurantPOJO() {
		super();
	}
	
	

	@Override
	public String toString() {
		return "RestaurantPOJO [id=" + id + ", name=" + name + ", url=" + url + ", address=" + address + ", cuisines="
				+ cuisines + "]";
	}

	public String getCuisines() {
		return cuisines;
	}
	
	public void setCuisines(String cuisines) {
		this.cuisines = cuisines;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
