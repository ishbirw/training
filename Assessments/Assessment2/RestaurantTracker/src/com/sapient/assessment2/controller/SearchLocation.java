package com.sapient.assessment2.controller;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.xml.*;

import org.glassfish.jersey.client.ClientConfig;

import com.sapient.assessment2.POJO.LocationPOJO;
import com.sapient.assessment2.POJO.RestaurantPOJO;
import com.sapient.assessment2.ZomatoDAO.LocationDAO;
/**
 * Servlet implementation class SearchLocation
 */
public class SearchLocation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchLocation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rs = request.getRequestDispatcher("Search.jsp");
		rs.forward(request, response); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String location = request.getParameter("location");
		
		LocationDAO ldao = new LocationDAO();
		
		LocationPOJO lpojo = ldao.searchLocation(location);
		
		System.out.println(lpojo);
		
		List<RestaurantPOJO> tmp = ldao.searchLocationDetails(lpojo);
		
		RequestDispatcher rs = request.getRequestDispatcher("RestaurantList.jsp");
		request.setAttribute("list", tmp);
		rs.forward(request, response); 
		
	}
}
