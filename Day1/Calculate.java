import java.util.*;

public class Calculate
{
	public static void main(String args[])
	{
		List<Calc> arr = new ArrayList<>();
		arr.add(0,new Sum());
		arr.add(1,new Minus());
		arr.add(2,new Divide());
		arr.add(3,new Multiply());
		 
		Scanner in = new Scanner(System.in);
		
		float a = in.nextFloat();
		float b = in.nextFloat();

		int choice = in.nextInt();
		
		Calc cc = arr.get(choice-1);
		cc.calc(a,b);
	}
}


/*
//could have done 
Calc cc[4];
cc[0] = (a,b) -> (a+b);
cc[1] = (a,b) -> (a-b);
cc[2] = (a,b) -> (a/b);
cc[3] = (a,b) -> (a*b);
cc[choice].calc(a,b);

*/


interface Calc{

	abstract void calc(float a, float b);
}


class Sum implements Calc{
	public void calc(float a, float b)
	{
		System.out.println(a+b);
	}
}


class Minus implements Calc{
	public void calc(float a, float b)
	{
		System.out.println(a-b);
	}
}


class Divide implements Calc{
	public void calc(float a, float b)
	{
		System.out.println(a/b);
	}
}


class Multiply implements Calc{
	public void calc(float a, float b)
	{
		System.out.println(a*b);
	}
}