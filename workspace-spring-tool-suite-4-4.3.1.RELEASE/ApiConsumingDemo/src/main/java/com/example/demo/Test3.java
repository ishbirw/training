package com.example.demo;
import java.util.Scanner;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

class Read{
    public static Scanner sc = new Scanner(System.in);
}
public class Test3 {

      static final String URL_CREATE_EMPLOYEE = "http://10.151.60.205:8099/student";
      
       public static void main(String[] args) {
     

          Student newEmployee = new Student();
          System.out.println("enter name, age, city");
          
          newEmployee.setName(Read.sc.nextLine());
          newEmployee.setAge(Read.sc.nextLine());
          newEmployee.setCity(Read.sc.nextLine());
          
          HttpHeaders headers = new HttpHeaders();
          headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
          headers.setContentType(MediaType.APPLICATION_JSON);
     
          RestTemplate restTemplate = new RestTemplate();
     
          // Data attached to the request.
          HttpEntity<Student> requestBody = new HttpEntity<>(newEmployee, headers);
     
          // Send request with POST method.
          String e =restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, String.class);
          System.out.println(e);
          System.out.println("Employee created: ");     
       }
}