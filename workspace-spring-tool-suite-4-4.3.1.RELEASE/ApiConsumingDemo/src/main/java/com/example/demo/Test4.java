package com.example.demo;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class Test4 {

      static final String URL_CREATE_EMPLOYEE = "http://10.151.60.205:8099/student";
      
       public static void main(String[] args) {
     

          System.out.println("enter name");
          String name;
          name = Read.sc.nextLine();
          
          
          HttpHeaders headers = new HttpHeaders();
          headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
          headers.setContentType(MediaType.APPLICATION_JSON);
     
          RestTemplate restTemplate = new RestTemplate();
     
          // Data attached to the request.
          HttpEntity<String> requestBody = new HttpEntity<>(name, headers);
          
          // Send request with POST method.
          
//          restTemplate.delete(URL_CREATE_EMPLOYEE, requestBody);
          String e = restTemplate.exchange(URL_CREATE_EMPLOYEE,HttpMethod.DELETE,requestBody,String.class).getBody();
          System.out.println(e);
       }
}