package com.example.demo;
import java.util.Arrays;
import java.util.Scanner;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class Test2 {
    static final String URL_EMPLOYEES = "http://10.151.60.205:8099/student";
     
   public static void main(String[] args) {

       HttpHeaders headers = new HttpHeaders();

       headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));

       headers.setContentType(MediaType.APPLICATION_JSON);

       HttpEntity<Student[]> entity = new HttpEntity<Student[]>(headers);

       RestTemplate restTemplate = new RestTemplate();

       ResponseEntity<Student[]> response = restTemplate.exchange(URL_EMPLOYEES, //
               HttpMethod.GET, entity, Student[].class);

       Student[] result = response.getBody();
       
       Arrays.stream(result).forEach(System.out::println);
   }

}
