package com.example.demo;

import java.io.Serializable;

public class Add implements Serializable{
	transient int num1;
	transient int num2;
	int num3;
	
	public void setData(int n1,int n2)
	{
		num1=n1;
		num2=n2;
	}
	
	public void cal()
	{
		num3= num1 +num2;
	}
	
	public void display()
	{
		System.out.println("Num1="+num1+", Num2="+num2+", Num3="+num3);
	}
}
