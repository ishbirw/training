package com.example.demo;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializationDemo {
	public static void main(String[] args) throws Exception {
		FileInputStream fin = new FileInputStream("test.dat");
		ObjectInputStream in = new ObjectInputStream(fin);
		
		Add ob = (Add)in.readObject();
		ob.display();
	}
}
