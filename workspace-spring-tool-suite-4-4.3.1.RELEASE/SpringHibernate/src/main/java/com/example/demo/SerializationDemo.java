package com.example.demo;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerializationDemo  {
	
	public static void main(String[] args) throws Exception{
		Add add = new Add();
		add.setData(100, 200);
		add.cal();
		
		FileOutputStream fout = new FileOutputStream("test.dat");
		ObjectOutputStream out = new ObjectOutputStream(fout);
		out.writeObject(add);
		out.close();
		fout.close();
		System.out.println("File is serialized");
		
		
		
	}
}
