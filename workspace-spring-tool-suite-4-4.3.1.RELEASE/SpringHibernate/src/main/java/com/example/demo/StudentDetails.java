package com.example.demo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="student")
public class StudentDetails {

	@Id
	private String regno;
	@Column
	private String fname;
	@Column
	private String lname;
	@Column
	private String dob;
	@Column
	private String citycode;
	
	@Transient
	@OneToMany(mappedBy = "regno")
	Set<StudentMarks> sm = new HashSet<>();
	
	public StudentDetails() {
		super();
	}
	public StudentDetails(String id, String fname, String lname, String dob, String citycode) {
		super();
		this.regno = id;
		this.fname = fname;
		this.lname = lname;
		this.dob = dob;
		this.citycode = citycode;
	}
	
	public Set<StudentMarks> getSm() {
		return sm;
	}
	public void setSm(Set<StudentMarks> sm) {
		this.sm = sm;
	}
	
	public String getRegno() {
		return regno;
	}
	public void setRegno(String regno) {
		this.regno = regno;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getCitycode() {
		return citycode;
	}
	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}
}
