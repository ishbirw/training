package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(value="prototype")
public class StudentList {
	private List<StudentBean> list;

	public StudentList() {
		super();
		list = new ArrayList<StudentBean>();
		list.add(new StudentBean("Ishbir",21,"New Delhi"));
		list.add(new StudentBean("Riya",17,"New Delhi"));
		list.add(new StudentBean("Samson",22,"Mumbai"));
		list.add(new StudentBean("Raunak",21,"Bangalore"));
		
	}

	public List<StudentBean> getList() {
		return list;
	}

	public void setList(List<StudentBean> list) {
		this.list = list;
	}
}
