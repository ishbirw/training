package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@Autowired
	StudentDAO ob;
	
	@GetMapping("/hello")
	public String display()
	{
		return "<h1>Hello moto</h1>";
	}
	
	@GetMapping("/hello1")
	public StudentBean display1()
	{
		return new StudentBean("Ishbir",21,"New Delhi");
	}
	
	@GetMapping("/student")
	public List<StudentBean> getStudents()
	{
		return ob.getStudents();
	}
	
	@GetMapping("/student/{name}")
	public List<StudentBean> getDetails(@PathVariable String name)
	{
		return ob.getDetails(name);
	}
	
	@PostMapping("/student")
	public String insert(@RequestBody StudentBean bean)
	{
		return ob.insert(bean);
	}
	
	@DeleteMapping("/delete/{name}")
	public String delete(@PathVariable String name)
	{
		return ob.delete(name);
	}
	
	@PostMapping("/update")
	public String update(@RequestBody StudentBean bean)
	{
		return ob.update(bean);
	}
	
	
}

