package com.example.demo;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentDAO {
	
	@Autowired
	StudentList l1;
	
	public List<StudentBean> getStudents()
	{
		return l1.getList();
	}
	
	public List<StudentBean> getDetails(String name)
	{
		return l1.getList().stream().filter(e->e.getName().equals(name)).collect(Collectors.toList());
	}
	
	public String insert(StudentBean ob)
	{
		l1.getList().add(ob);
		return "Done";
	}
	
	public String delete(String name)
	{
		l1.getList().removeIf(s->s.getName().equals(name));
		return "Removed";
	}
	
	public String update(StudentBean bean)
	{
		l1.getList().stream().filter(s->s.getName().equals(bean.getName())).collect(Collectors.toList()).get(0).setStudent(bean);
		return "Updated";
	}
	
}
