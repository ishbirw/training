package com.example.demo;

public class StudentBean {
	private String name;
	private int age;
	private String city;
	
	
	public StudentBean(String name, int age, String city) {
		super();
		this.name = name;
		this.age = age;
		this.city = city;
	}


	public StudentBean() {
		super();
	}
	
	public void setStudent(StudentBean bean)
	{
		this.age = bean.age;
		this.city = bean.city;
		this.name = bean.name;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}
	
	
	
}
