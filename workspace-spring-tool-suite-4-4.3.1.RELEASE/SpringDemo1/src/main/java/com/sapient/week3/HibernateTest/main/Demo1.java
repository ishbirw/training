package com.sapient.week3.HibernateTest.main;

import java.util.List;


import org.hibernate.Session;
import org.hibernate.Transaction;

import com.sapient.week3.HibernateTest.Entity.Employee;
import com.sapient.week3.HibernateTest.util.HibernateUtil;


public class Demo1 {
	
	public static void main(String[] args) {
        Employee student = new Employee(3,"Samson",22,1000f);
        Employee student1 = new Employee(4,"Snigdha",22,1200f);
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the student objects
            session.save(student);
            session.save(student1);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            List < Employee > students = session.createQuery("from Employee", Employee.class).list();
            students.forEach(s -> System.out.println(s.getName()));
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
	
}
