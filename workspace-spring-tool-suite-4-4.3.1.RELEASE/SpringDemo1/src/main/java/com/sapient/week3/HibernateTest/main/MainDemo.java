package com.sapient.week3.HibernateTest.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sapient.week3.HibernateTest.Entity.Employee;

public class MainDemo {
	
	public static void main(String[] args) {
		try {
		ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		EmployeeDAO empDao;
		
		empDao = (EmployeeDAO)context.getBean("employeedao");
		
//		empDao.insertEmployee(new Employee("Karan",22,800));
//		empDao.insertEmployee(new Employee("Riya",17,1000));
		empDao.updateSalaryByID(1);
		empDao.getEmployeeByName("Ishbir").forEach(s->System.out.println(s));
		
		
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
