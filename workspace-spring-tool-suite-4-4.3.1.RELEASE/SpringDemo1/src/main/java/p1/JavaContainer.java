package p1;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JavaContainer {
	
	@Bean
	public Hello get1(){
		return new Hello(); // even though new here - will save it in configuration - will use singleton scope by default
	}
	
	@Bean
	public Holiday get2()
	{
		return new Holiday("15/07/2019","New Year");
	}
	
	@Bean
	public Employee get3()
	{
		Employee ob;
		ob = new Employee(21,"Ishbir");
		return ob;
	}
	
	
	@Bean
	public HolidayList get4()
	{
		HolidayList ob;
		ob = new HolidayList(new ArrayList<Holiday>());
		ob.getHolidayList().add(new Holiday("15/07/2019","new"));
		ob.getHolidayList().add(new Holiday("15/07/2019","bad"));
		ob.getHolidayList().add(new Holiday("15/07/2019","old"));
		return ob;
	}
	
	
	
}
