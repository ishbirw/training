package p1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo1 {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		
		System.out.println("Task1");
		Hello ob;
		ob = (Hello)context.getBean("hello");
		ob.display();
		
		System.out.println("Task2");
		Holiday hh;
		hh = (Holiday)context.getBean("holiday");
		System.out.println(hh);
		
		System.out.println("Task3");
		HolidayList hlist;
		hlist = (HolidayList)context.getBean("holiday_list");
		hlist.display();
		
		
		System.out.println("Task4");
		Employee emp;
		emp = (Employee)context.getBean("employee");
		System.out.println(emp);
		
		
		((ClassPathXmlApplicationContext)context).close();
	}
}
