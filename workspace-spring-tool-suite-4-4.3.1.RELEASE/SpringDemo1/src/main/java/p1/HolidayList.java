package p1;

import java.util.List;

public class HolidayList {
	private List<Holiday> holidayList;
	
	public HolidayList() {
		super();
	}

	public HolidayList(List<Holiday> holidayList) {
		super();
		this.holidayList = holidayList;
	}

	public List<Holiday> getHolidayList() {
		return holidayList;
	}

	public void setHolidayList(List<Holiday> holidayList) {
		this.holidayList = holidayList;
	}

	@Override
	public String toString() {
		
		return "HolidayList [holidayList=" + holidayList + "]";
	}
	
	public void display()
	{
		holidayList.forEach(s->System.out.println(s));
	}
	
	
}
