package com.sapient.rest.server;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hi")
public class Student {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getString1()
	{
		return "Hilo from REST";
	}

}
