package com.sapient.student.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.student.bean.StudentBean;
import com.sapient.student.exceptions.InputFieldEmptyException;
import com.sapient.student.others.RBundle;

/**
 * Servlet implementation class StudentDeleteController
 */
public class StudentDeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentDeleteController() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    ServletContext cn;
    @Override
    public void init(ServletConfig config) throws ServletException {
    	cn= config.getServletContext();
    	super.init(config);
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		Map<String,StudentBean> studList = (Map<String,StudentBean>)cn.getAttribute("smlist");
		
		String id = request.getParameter("id1");
		String name = request.getParameter("id2");
		String rollno = request.getParameter("id3");
		String percent = request.getParameter("id4");
		
		studList.remove(id);
		
		cn.setAttribute("smlist", studList);
		
		RequestDispatcher rs = request.getRequestDispatcher("DeleteEdit.jsp");
		rs.forward(request, response);
	
	}

}
