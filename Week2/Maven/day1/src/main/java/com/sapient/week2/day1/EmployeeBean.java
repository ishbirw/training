package com.sapient.week2.day1;

public class EmployeeBean {
	private int id;
	private String name;
	private float salary;
	
	public EmployeeBean() {
		super();
	}
	public EmployeeBean(int id, String name, float salary) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "EmployeeBean [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		EmployeeBean tmp = (EmployeeBean) obj;
		
		if(this.getId()==tmp.getId() && this.getName().equals(tmp.getName()) && this.getSalary() == tmp.getSalary())
		{
			return true;
		}
		else
		{
			return super.equals(obj);
		}
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
}
