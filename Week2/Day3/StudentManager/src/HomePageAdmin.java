

import java.util.List;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.week2.day3.student.Student;
import com.sapient.week2.day3.student.StudentDAO;

/**
 * Servlet implementation class HomePage
 */
public class HomePageAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomePageAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Cookie cookies[]=request.getCookies();
		Cookie cookie = null;
		for(Cookie ck : cookies )
		{
			if(ck.getName().equals("user_studentManager"))
			{
				cookie = ck;
			}
		}
		
		if(cookie==null || cookie.getValue().isEmpty())
		{
			return;
		}
		else if(!cookie.getValue().split("_")[1].equals("1"))
		{
			return;
		}
		
		PrintWriter out = response.getWriter();
		StudentDAO studDao;
		try
		{
			studDao = new StudentDAO();
			
			out.print("<html><body><h1>Welcome from get</h1>");
			
			this.displayStudent(out, studDao.getStudents());
			out.print("</form></body></html>");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try
		{
			out.print("<html><body><h1>Welcome from post</h1>");			
			out.print("</form></body></html>");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void displayStudent(PrintWriter out,List<Student> studentList)
	{
		for(Student stud: studentList)
		{
			out.print("Student Name ="+stud.getName()+", RollNo= "+stud.getRollno()+", Marks= "+stud.getMarks()+" <br>");
			out.print("<hr size='2' width='100%' align='left' color='green'>");
		}
	}

}
