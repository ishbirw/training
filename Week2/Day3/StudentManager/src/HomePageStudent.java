

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.week2.day3.student.Student;
import com.sapient.week2.day3.student.StudentDAO;

/**
 * Servlet implementation class HomePageStudent
 */
public class HomePageStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomePageStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Cookie cookies[]=request.getCookies();
		Cookie cookie = null;
		for(Cookie ck : cookies )
		{
			if(ck.getName().equals("user_studentManager"))
			{
				cookie = ck;
			}
		}
		
		if(cookie==null || cookie.getValue().isEmpty())
		{
			return;
		}
		else if(!cookie.getValue().split("_")[1].equals("2"))
		{
			return;
		}
		int uname_curr = Integer.parseInt(cookie.getValue().split("_")[0]);
		StudentDAO studDao;
		PrintWriter out = response.getWriter();
		
		String uname=null, marks=null;
		uname = request.getParameter("uname");
		marks = request.getParameter("marks");
		
		
		try
		{
			studDao = new StudentDAO();
			Student curr = studDao.getStudentsFromID(uname_curr);
			
			if(uname==null || uname.isEmpty())
			{
				uname = ""+uname_curr;
				marks = ""+ curr.getMarks();
			}
			else
			{
				Student temp = studDao.getStudentsFromID(Integer.parseInt(uname));
				marks = ""+ temp.getMarks();
				
			}
			
			out.print("<html><body><h1>Welcome Student "+curr.getName()+"</h1>");
			out.print("<form action='HomePageStudent' method='GET'>");
			out.print("Enter Username<input type='text' name='uname' value='"+uname+"' ><br>");
			out.print("Marks =<input type='text' name='marks' value='"+marks+"' ><br>");
			out.print("<input type='submit' value='submit'></br>");			
			out.print("</form>");
			out.print("<form action='Logout' method='GET'>");
			out.print("<input type='submit' value='logout'></br>");
			out.print("</form></body></html>");
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
