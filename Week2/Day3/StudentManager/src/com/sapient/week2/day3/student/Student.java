package com.sapient.week2.day3.student;

public class Student {
	private String name;
	private float marks;
	private int rollno;
	
	public Student() {
		super();
	}

	public Student(int rollno, String name, float marks) {
		super();
		this.name = name;
		this.marks = marks;
		this.rollno = rollno;
	}
	
	@Override
	public String toString() {
		return "Student [name=" + name + ", marks=" + marks + ", rollno=" + rollno + "]";
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getMarks() {
		return marks;
	}
	public void setMarks(float marks) {
		this.marks = marks;
	}
	public int getRollno() {
		return rollno;
	}
	public void setRollno(int rollno) {
		this.rollno = rollno;
	}
}
