package com.sapient.week2.day3.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.sapient.week2.day3.student.Student;

public class Authenticate {
	
	public Boolean auth(String uname,String password,int mode) throws Exception
	{
		Connection co = DBConnect.getConnection();
		PreparedStatement statement; 
		String tableName = "";
		if(mode==1)
		{
			tableName = "adminUser";
			statement = co.prepareStatement("select id,password from adminUser where id=?");
		}
		else
		{
			tableName = "studentUser";
			statement = co.prepareStatement("select id,password from studentUser where id=?");
		}
		
		statement.setInt(1, Integer.parseInt(uname));
		ResultSet res = statement.executeQuery();
		Boolean result = false;
		while(res.next())
		{
			
			if(res.getString(2).equals(password))
			{
				result = true;
				break;
			}
		}
		return result;
	}
}
