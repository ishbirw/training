package com.sapient.week2.day3.student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import com.sapient.week2.day3.model.*;

public class StudentDAO {
	
	public List<Student> getStudents() throws Exception
	{
		Connection co = DBConnect.getConnection();
		PreparedStatement statement = co.prepareStatement("select rollno,name,marks from student");
		ResultSet res = statement.executeQuery();
		List<Student> studentList = new ArrayList<>();
		
		while(res.next())
		{
			studentList.add(new Student(res.getInt(1),res.getString(2),res.getFloat(3)));
		}
		
		return studentList;
	}
	
	public Student getStudentsFromID(int rollno) throws Exception
	{
		Connection co = DBConnect.getConnection();
		PreparedStatement statement = co.prepareStatement("select rollno,name,marks from student where rollno=?");
		statement.setInt(1,rollno);
		ResultSet res = statement.executeQuery();
		List<Student> studentList = new ArrayList<>();
		
		while(res.next())
		{
			studentList.add(new Student(res.getInt(1),res.getString(2),res.getFloat(3)));
		}
		if(studentList.size()>0)
		{
			return studentList.get(0);
		}
		return null;
	}
	
	public int insertPlayer(Student student) throws Exception
	{
		Connection co = DBConnect.getConnection();
		PreparedStatement statement = co.prepareStatement("insert into student values(?,?,?)");
		statement.setInt(1, student.getRollno());
		statement.setString(2, student.getName());
		statement.setFloat(3, student.getMarks());
		
		return statement.executeUpdate();		
	}
	

	public int deletePlayer(int rollno) throws Exception 
	{
		Connection co = DBConnect.getConnection();
		PreparedStatement statement = co.prepareStatement("delete from student where rollno=?");
		statement.setInt(1, rollno);
		
		return statement.executeUpdate();
	}
	
	public int updatePlayer(Student student) throws Exception
	{
		Connection co = DBConnect.getConnection();
		PreparedStatement statement = co.prepareStatement("update player set name=?,marks=? where rollno=?");
		statement.setString(1, student.getName());
		statement.setFloat(2, student.getMarks());
		statement.setInt(3, student.getRollno());
		
		return statement.executeUpdate();		
	}
}
