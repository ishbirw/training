

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.week2.day3.model.Authenticate;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Cookie cookies[]=request.getCookies();
		Cookie cookie = null;
		
		for(Cookie ck : cookies )
		{
			if(ck.getName().equals("user_studentManager"))
			{
				cookie = ck;
			}
		}
		
		if(cookie!=null && !cookie.getValue().isEmpty())
		{
			int mode = Integer.parseInt(cookie.getValue().split("_")[1]);
			if(mode==1)
			{
				response.sendRedirect("/StudentManager/HomePageAdmin");
			}
			else
			{
				response.sendRedirect("/StudentManager/HomePageStudent");
			}
			
		}
		
		
		PrintWriter out = response.getWriter();
		try
		{
			out.print("<html><body><h1>Login Page</h1>");
			out.print("<form action='Login' method='POST'>");
			out.print("Enter Username<input type='text' name='uname'><br>");
			out.print("Password <input type='password' name='password'><br>");
			out.print("<input type='radio' name='radio' value='1'>Admin</br>");
			out.print("<input type='radio' name='radio' value='2' checked>Student</br>");
			out.print("<input type='submit' value='submit'></br>");
			
			
			out.print("</form></body></html>");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String uname = request.getParameter("uname");
		String password = request.getParameter("password");
		int valSelect = Integer.parseInt(request.getParameter("radio"));
		
		PrintWriter out = response.getWriter();

		try
		{
			if(uname.isEmpty() || password.isEmpty())
			{
				out.print("<html><body><h1>Login Page</h1>");
				out.print("<form action='Login' method='POST'>");
				out.print("Enter Username<input type='text' name='uname' ><br>");
				out.print("Password <input type='password' name='password' ><br>");
				out.print("<input type='radio' name='radio' value='1'>Admin</br>");
				out.print("<input type='radio' name='radio' value='2' checked>Student</br>");
				out.print("<input type='submit' value='submit'></br>");
				
				out.print("<script>alert('Enter the fields correctly');</script>");
				
				out.print("</form></body></html>");
			}
			else
			{
				Authenticate auth = new Authenticate();
				if(auth.auth(uname, password, valSelect))
				{
					
					Cookie cookie = new Cookie("user_studentManager", uname+"_"+valSelect);
					response.addCookie(cookie);
					if(valSelect==1)
					{
						response.sendRedirect("/StudentManager/HomePageAdmin");
					}
					else if(valSelect==2)
					{
						response.sendRedirect("/StudentManager/HomePageStudent");
					}
				}
				else
				{
					out.print("<html><body><h1>Login Page</h1>");
					out.print("<form action='Login' method='POST'>");
					out.print("Enter Username<input type='text' name='uname' ><br>");
					out.print("Password <input type='password' name='password' ><br>");
					out.print("<input type='radio' name='radio' value='1'>Admin</br>");
					out.print("<input type='radio' name='radio' value='2' checked>Student</br>");
					out.print("<input type='submit' value='submit'></br>");
					
					out.print("<script>alert('The username or password entered is not correct');</script>");
					
					out.print("</form></body></html>");
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
	}

}
