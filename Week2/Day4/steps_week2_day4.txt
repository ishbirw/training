Lazy load - load resources when needed( not at start)


for transfering data between servlets use - request object 
For storing data across sessions - use sessions
For giving access to multiple sessions and multiple users - use context -> not good when large data 
-> so better to use CLUSTERING 

-> should have only one Controller
The lifecycle of servlet ->

	1. init() method is called only once in the lifecycle of the servlet. -> only once (so should add the data loading here)
	2. Constructor and service method (create thread of service) for every new request.
	
BO -> Business object - its an enterprise bean -> for business logic / operations on the bean
DAO -> should return MAP now as it will be called once in the init() method so will give fast read access.
TreeMap - sorted on keys

How to get column names -> ResultSetMetaData = ResultSet.getMetaData() -> called after execute() -> gives the column names 
ResultSet = it can only move in forward direction so can't call rs.getString(1) twice -> as can't go back
	-> if resultset is made scrollable , then can move anywhere 

Recommended way to use POJO  with JDBC
	-> Keep data in strings no need to typecast it. Only typecast when need to do some calculations
	-> and use Data warehousing
	

After init()
	-> come to doPost() as more secure than get - data hidden
	
Database Admin Rules to follow
	-> No one should know the table name nor the column name 
	-> Should use views 

Application scope -> across 2 applications  -> across projects
Context scope -> across multiple servlets -> inside project (accessible to all sessions/users)
Session scope -> Across sessions for a particular user
request scope -> for that request object only.

	
Property file Advantages 
	-> should be under the SRC or under WEB CONTENTS
	-> Need to update at only one place
	-> Can be updated at run time on server [Best Advantage]

HTMl
	-> <% %> tag to do java stuff - before <body> tag

RESTful services - jersey


	
