package com.sapient.student.exceptions;

public class InputFieldEmptyException extends Exception{

	public InputFieldEmptyException() {
		super();
	}

	public InputFieldEmptyException(String message) {
		super(message);
	}

	
}
