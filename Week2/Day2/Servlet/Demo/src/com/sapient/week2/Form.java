package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Form
 */
public class Form extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Form() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		try
		{
			out.print("<html><body><h1>Adding 2 numbers</h1>");
			out.print("<form action='Form' method='POST'>");
			out.print("Enter no.1 <input type='text' name='n1' value='0'><br>");
			out.print("Enter no.2 <input type='text' name='n2' value='0'><br>");
			out.print("Result <input type='text' name='n3' value='0'><br>");
			out.print("<input type='submit' value='submit'></br>");
			out.print("<input type='radio' name='radio' value='1' checked>Add</br>");
			out.print("<input type='radio' name='radio' value='2'>Subtract</br>");
			out.print("<input type='radio' name='radio' value='3'>Multiply</br>");
			
			
			out.print("</form></body></html>");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int num1 = Integer.parseInt(request.getParameter("n1"));
		int num2 = Integer.parseInt(request.getParameter("n2"));
		int valSelect = Integer.parseInt(request.getParameter("radio"));
		
		int result=0;
		if(valSelect==1)
		{
			result = num1 + num2;
		}
		else if(valSelect==2)
		{
			result = num1 - num2;
		}
		else if(valSelect==3)
		{
			result = num1 * num2;
		}
		
		PrintWriter out = response.getWriter();
		try
		{
			out.print("<html><body><h1>Adding 2 numbers</h1>");
			out.print("<form action='Form' method='POST'>");
			out.print("Enter no.1 <input type='text' name='n1' value='"+num1+"'><br>");
			out.print("Enter no.2 <input type='text' name='n2' value='"+num2+"'><br>");
			out.print("Result <input type='text' name='n3' value='"+result+"'><br>");
			out.print("<input type='submit' value='submit'><br>");
			out.print("<input type='radio' name='radio' value='1' checked>Add</br>");
			out.print("<input type='radio' name='radio' value='2'>Subtract</br>");
			out.print("<input type='radio' name='radio' value='3'>Multiply</br>");
			
			
			out.print("</form></body></html>");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

}
