package com.sapient.week2.day1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class EmployeeDAOTest {
	
	EmployeeDAO empDAO;
	List<EmployeeBean> empList ;
	
	@Before
	public void init()
	{
		empDAO = new EmployeeDAO();
		try
		{
			empList = empDAO.readData();
		}
		catch(FileNotFoundException e)
		{
			System.err.println("File not found");
			e.getStackTrace();
		}
		catch (IOException e) {
			System.err.println("IO exception");
			e.printStackTrace();
		}
	}
	
	@Test
	public void dataLoadingTest()
	{
		assertEquals(5,empList.size());
	}
	
	@Test
	public void totalSalaryTest()
	{
		Float expectedSal = 1000f+1000+3000+4000+5000; 
		Float actualSal  = empDAO.getTotSal(empList);
		assertEquals(expectedSal,actualSal);
	}
	
	@Test
	public void salaryCountTest()
	{
		int querySalary = 1000;
		int expectedCount = 2;
		int actualCount = empDAO.getCount(empList, querySalary);
		assertEquals(expectedCount,actualCount);
	}
	
	@Test
	public void searchByIDTest()
	{
		int queryID = 3;
		EmployeeBean expectedBean = new EmployeeBean(queryID,"Samson",3000);
		EmployeeBean testBean =  empDAO.getEmployee(queryID);
		Boolean isCorrect;
		
		if(expectedBean.equals(testBean))
		{
			isCorrect=true;
		}
		else
		{
			isCorrect = false;
		}
		assumeTrue(isCorrect);
	}
	
}
