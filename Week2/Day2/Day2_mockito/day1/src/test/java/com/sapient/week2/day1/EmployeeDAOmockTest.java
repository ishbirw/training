package com.sapient.week2.day1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;
import static org.mockito.Mockito.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class EmployeeDAOmockTest {
	static EmployeeDAO mockEmpDao;
	static List<EmployeeBean> empList;
	
	@BeforeClass
	public static void init()
	{
		mockEmpDao = mock(EmployeeDAO.class);
		empList = new ArrayList<>();
		empList.add(new EmployeeBean(1,"Ishbir",1000));
		empList.add(new EmployeeBean(2,"Karan",1000));
		empList.add(new EmployeeBean(3,"Samson",3000));
		empList.add(new EmployeeBean(4,"Gurpal",4000));
		empList.add(new EmployeeBean(5,"Rajat",5000));
		try {
			when(mockEmpDao.readData()).thenReturn(empList);
			when(mockEmpDao.getTotSal(anyListOf(EmployeeBean.class))).thenCallRealMethod();
			when(mockEmpDao.getCount(anyListOf(EmployeeBean.class), anyInt())).thenCallRealMethod();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Before
	public void init1()
	{
		try {
			empList = mockEmpDao.readData();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void dataLoadingTest()
	{
		assertEquals(5,empList.size());
	}
	
	@Test
	public void totalSalaryTest()
	{
		Float expectedSal = 1000f+1000+3000+4000+5000; 
		Float actualSal  = mockEmpDao.getTotSal(empList);
		assertEquals(expectedSal,actualSal);
	}
	
	@Test
	public void salaryCountTest()
	{
		int querySalary = 1000;
		int expectedCount = 2;
		int actualCount = mockEmpDao.getCount(empList, querySalary);
		assertEquals(expectedCount,actualCount);
	}
	
//	@Test
//	public void searchByIDTest()
//	{
//		int queryID = 3;
//		EmployeeBean expectedBean = new EmployeeBean(queryID,"Samson",3000);
//		EmployeeBean testBean =  mockEmpDao.getEmployee(queryID);
//		Boolean isCorrect;
//		
//		if(expectedBean.equals(testBean))
//		{
//			isCorrect=true;
//		}
//		else
//		{
//			isCorrect = false;
//		}
//		assumeTrue(isCorrect);
//	}
	
}
