package com.sapient.week2.day1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmployeeDAO {
	
	private Map<Integer, EmployeeBean> empMap;
	
	public List<EmployeeBean> readData() throws FileNotFoundException,IOException
	{
		List<EmployeeBean> empList = new ArrayList<>();
		this.empMap = new HashMap<>();
		
		File file = new File("C:\\\\Users\\\\ishwalia\\\\Desktop\\\\Workspace\\\\project-week1\\\\src\\\\com\\\\sapient\\\\week2\\\\day1\\\\Assessment1\\\\Employee.csv");
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = "";
		int lineNumber = 0;
		while((line = br.readLine()) != null)
		{
			lineNumber+=1;
			String tmp[] = line.split(",");
			
			if(tmp.length != 3)
			{
				System.out.println("CSV file not correct at line number = "+lineNumber);
				System.exit(0);
			}
			EmployeeBean tmpEmployee =  new EmployeeBean(Integer.parseInt(tmp[0]),tmp[1],Float.parseFloat(tmp[2])); 
			empList.add(tmpEmployee);
			this.empMap.put(tmpEmployee.getId(), tmpEmployee);
		}
		br.close();
		return empList;
	}
	
	public float getTotSal(List<EmployeeBean> empList)
	{
		return empList.stream().map(x->x.getSalary()).reduce(0.0f,Float::sum);
		
	}
	
	public int getCount(List<EmployeeBean> empList,int salary)
	{
		return (int) empList.stream().filter((s)-> s.getSalary()==salary).count();
	}
	
	public EmployeeBean getEmployee(int id)
	{
		return this.empMap.get(id); 
	}
}
