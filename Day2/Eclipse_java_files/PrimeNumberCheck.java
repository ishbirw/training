package com.sapient.week1;

// A prime number is a number which has only two factors - 1 and itself 
public class PrimeNumberCheck {

	public Boolean check(int number) {
		
		if(number<=0)
		{
			return false;
		}
		
		if(number<=3 && number>=1)
		{
			return true;
		}
		if(number%2==0 || number%3==0)
		{
			return false;
		}
		
		for(int i=5;i*i<number;i+=6)
		{
			if(number%i==0 || number%(i+2)==0)
			{
				return false;
			}
		}
		return true;
	}

}
