package com.sapient.week1;

public class Matrix {
	int mat[][];
	int n;
	int m;
	
	public Matrix() {
		mat = new int[3][3];
		n = 3;
		m = 3;
	}
	
	public Matrix(Matrix mat)
	{
		this.mat = mat.mat;
		this.n = mat.n;
		this.m = mat.m;
	}
	
	public void adoptMatrix(int input_matrix[][])
	{
		mat = input_matrix;
		n = input_matrix.length;
		m = input_matrix[0].length;
	}
	
	public Matrix(int n, int m) {
		mat = new int[n][m];
		this.n = n;
		this.m = m;
	}
	
	public void read()
	{
		System.out.println("Enter the matrix of size "+ n +" rows and "+m+" columns");
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<m;j++)
			{
				this.mat[i][j] = Read.sc.nextInt();
			}
		}
	}
	
	public Matrix subtract(Matrix second_mat)
	{
		if(this.n != second_mat.n || this.m != second_mat.m)
		{
			System.out.println("Matricies are not of same size");
			return null;
		}
		Matrix output = new Matrix(this.n,this.m);
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<m;j++)
			{
				output.mat[i][j] = mat[i][j] - second_mat.mat[i][j];
			}
		}
		return output;
	}
	
	public void multiply(Matrix second_matrix)
	{
		if(this.m != this.n)
		{
			System.out.println("Matrices are not compatible for multiplication");
			return;
		}
		
	}
	
}
