package com.sapient.week1;

public class FigureToWords {
	
	public static String getWords(long amt)
	{
		String word = "";
		String[] unit = {"","One","Two","Three","Four","Five","Six",
				"Seven","Eight","Nine","Ten","Eleven","Twelve","Thirteen","Fourteen",
				"Fifteen","Sixteen","Seventeen","Eighteen","Nineteen"};
		String[] tens = {"","Ten","Twenty","Thirty","Fourty","Fifty","Sixty",
				"Seventy","Eighty","Ninety"};
		
		String[] vunits = {"Crore","Lakhs","Thousands","Hundred","only"};
		
		Long[] values = {10000000L,100000L,1000L,100L,1L}; 
		
		for(int i=0;i<values.length;i++)
		{
			int n = (int)(amt/values[i]);
			amt = amt%values[i];
			if(n>0)
			{
				if(n>19)
				{
					word += tens[n/10] +" "+ unit[n%10] +" "+ vunits[i] +" ";
				}
				else
				{
					word += unit[n] +" "+ vunits[i] +" ";
				}
			}
		}
		
		return word;
	}
	
	public static void main(String[] args) {
		System.out.println(FigureToWords.getWords(9821));
	}
}
