package com.sapient.week1;

import static org.junit.Assert.assertEquals;
import org.junit.*;


public class PrimeNumberCheckTest {

	PrimeNumberCheck primeCheck ;
	
	@Before
	public void init()
	{
		primeCheck = new PrimeNumberCheck();
	}
	
	@Test
	public void NumberisPrime()
	{
		assertEquals(true, primeCheck.check(7));
	}
	
	@Test
	public void NumberisNotPrime()
	{
		assertEquals(false, primeCheck.check(10));
	}
	@Test
	public void NumberisNegative()
	{
		assertEquals(false, primeCheck.check(-1));
	}
	
	
}
