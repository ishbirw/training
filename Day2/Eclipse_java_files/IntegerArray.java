package com.sapient.week1;

import java.util.Arrays;

public class IntegerArray {
	int size ;
	int array[];
	
	public IntegerArray(int[] arr,int siz) {
		this.array = arr;
		this.size = size;
	}
	
	public IntegerArray(IntegerArray intarr)
	{
		this.array = intarr.array;
		this.size = intarr.size;
	}
	
	public IntegerArray() {
		this.array = new int[10];
		this.size = 10;
	}
	
	public IntegerArray(int size)
	{
		this.array = new int[size];
		this.size = size;
	}
	
	public void adoptArray(int[] arr)
	{
		this.array = arr;
		this.size = arr.length;
	}
	
	public void display()
	{
		Arrays.stream(this.array).forEach(str-> System.out.print(str +","));
	}
	
	public void sort()
	{
		Arrays.sort(this.array);
	}
	
	public double average()
	{
		return Arrays.stream(this.array).average().getAsDouble();
	}
	
	public int search(int a)
	{
		int count = 0;
		for(int i: this.array)
		{
			if(i==a)
			{
				count++;
			}
		}
		return count;
	}
	
	public static void main(String[] args) {
		System.out.println("Enter the size of the array");
		int size = Read.sc.nextInt();
		int arr[] = new int[size];
		for(int i=0;i<size;i++)
		{
			arr[i] = Read.sc.nextInt();
		}
		
		IntegerArray i_arr = new IntegerArray(arr,size);
		System.out.println("\nThe array is equal to =");
		i_arr.display();
		
		System.out.println("\nThe sorted array is equal to =");
		i_arr.sort();
		i_arr.display();
		
		System.out.println("\nThe average is equal to =");
		System.out.println(i_arr.average());
		
	}
	
	
}
