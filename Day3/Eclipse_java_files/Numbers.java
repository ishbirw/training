package com.sapient.week1.day3;

public class Numbers {
	private int number;
	
	public Numbers(int num) {
		this.number = num;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	public void finalize()
	{
		number = 0;
		System.out.println("Number removed");
	}
}
