package com.sapient.week1.day3;

import java.util.ArrayList;
import java.util.Collections;

public class EmployeeList {
	
	public static void main(String[] args) {
		ArrayList<Employee> empList = new ArrayList<Employee>();
		
		empList.add(new Employee(1,"Ishbir","Delhi",21));
		empList.add(new Employee(2,"Gurpal","Gurgaon",22));
		empList.add(new Employee(3,"Karan","Delhi",20));
		
		System.out.println("Sorting based on Age");
		Employee.setSortMode(Employee.BY_AGE);
		Collections.sort(empList);
		empList.stream().forEach(s->s.displayEmployee());
		
		System.out.println("\nSorting based on Name\n");
		Employee.setSortMode(Employee.BY_NAME);
		Collections.sort(empList);
		empList.stream().forEach(s->s.displayEmployee());
	}
}
