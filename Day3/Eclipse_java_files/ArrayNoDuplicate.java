package com.sapient.week1.day3;

import java.util.Arrays;

public class ArrayNoDuplicate {
	int[] array;
	
	public ArrayNoDuplicate(int[] input_array) {
		array = Arrays.stream(input_array).distinct().toArray();
	}
	public void displayArray()
	{
		Arrays.stream(array).forEach(s->System.out.print(s+","));
	}
}
