package com.sapient.week1.day3;

public class GenericClassExample <T> {
	T elem;
	
	public GenericClassExample(T in_elem) {
		elem = in_elem;
	}
	
	public void display()
	{
		System.out.println(elem);
	}
	
	public static void main(String[] args) {
		GenericClassExample<String> stringTest = new GenericClassExample<String>("Ishbir");
		GenericClassExample<Integer> intTest = new GenericClassExample<Integer>(10);
		
		stringTest.display();
		intTest.display();
	}
}
