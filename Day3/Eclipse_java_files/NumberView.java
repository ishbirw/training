package com.sapient.week1.day3;

import java.util.Scanner;

public class NumberView {
	public Scanner sc;
	public NumberView() {
		sc = new Scanner(System.in);
	}
	public void displayNumber(Numbers number)
	{
		System.out.println("The number is = "+ number.getNumber());
	}
	
	public int readNumber()
	{
		System.out.println("Enter the number");
		return this.sc.nextInt();
	}
}
