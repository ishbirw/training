package com.sapient.week1.day3;

public class Controller {
	public static void main(String[] args) {
		NumberView numView = new NumberView();
		Numbers num1 = new Numbers(numView.readNumber());
		Numbers num2 = new Numbers(numView.readNumber());
		NumberDAO num_dao = new NumberDAO();
		numView.displayNumber(num_dao.addNumber(num1, num2));
		
		num1 = null;
		num2 = null;
		numView = null;
		num_dao = null;
		System.gc();
	}
}
