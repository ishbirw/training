package com.sapient.week1.day3;

public class Employee implements Comparable<Employee>{
	static final int BY_NAME = 1;
	static final int BY_AGE = 0;
	
	static Integer sortMode = 0; 
	
	private int id;
	private String name;
	private String address;
	private int age;
	
	
	public Employee(int eid, String ename,String eaddr,int eage) {
		this.id = eid;
		this.name = ename;
		this.address = eaddr;
		this.age = eage;
	}
	
	public static void setSortMode(int mode)
	{
		Employee.sortMode = mode;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public void displayEmployee()
	{
		System.out.println(this.id +","+ this.name +","+ this.address +","+ this.age);
	}
	
	@Override
	public int compareTo(Employee o) {
		
		if(Employee.sortMode == Employee.BY_AGE)
		{
			if(this.getAge() == o.getAge())
			{
				return 0;
			}
			else if(this.getAge() > o.getAge())
			{
				return 1;
			}
			else 
			{
				return -1;
			}
		}
		else
		{
			int val = this.getName().compareTo(o.getName());
			if(val == 0)
			{
				return 0;
			}
			else if(val > 0)
			{
				return 1;
			}
			else
			{
				return -1;
			}
			
		}
	}
	
	
	
}
