If the objects are not disposed then it could lead to memory leakage.
	- so need to derefer the object 
	- can set object to null , eg obj = null;
	- automatic memory management - without using delete. (auto collection)
	
finalize is the destructor in java 

new is executed by the garabage collector process.

system.gc() -  will run the destructor(finalize) - for all the dereferenced objects 


-> collections
We need collections for storing the data (processing not imp)


Collection is madeup of
- interfaces  - to implement run time polymorphism 
- classes  
- algos - to perform minimum work - optimizations 

Collection (Value) and Map(key and value)

interfaces - list, queue , dequeue , stack

vector - synchronized 
Arraylist - can handle asynchronous calls

linked list >> array list in terms of effeciency 


Set - unique 

hashset - unordered 
linkedhashset - ordered 
treeset - sorted 


checked  exceptions 
unchecked exceptions - divide by zero - compiler doesn't foce you to use try catch
