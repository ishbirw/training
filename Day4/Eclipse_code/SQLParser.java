package com.sapient.week1.day4;

import java.util.ArrayList;
import java.util.List;

public class SQLParser {
	ArrayList<String> rowSelect;
	EmployeeDataStore empStore;
	
	public SQLParser(EmployeeDataStore store) {
		rowSelect = new ArrayList<>();
		empStore = store;
	}
	
	public void Parse(String query)
	{
		String select = query.split(" ")[0];
		if(select.equalsIgnoreCase("select")==false)
		{
			System.out.println("Invalid Query");
		}
		String temp[] = query.split(" "); 
		String tmp = "";
		for(int i=1;i<temp.length;i++)
		{
			tmp+=temp[i];
		}
		for(String str: tmp.split(","))
		{
			rowSelect.add(str);
		}	
		
		empStore.displayEmployee(rowSelect);
	}
}
