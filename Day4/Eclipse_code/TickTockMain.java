package com.sapient.week1.day4;

public class TickTockMain {
	
	public static void main(String[] args) {
		
		TickTock ticktock = new TickTock();
		
		Thread tick = new Thread(new Tick(ticktock));
		Thread tock = new Thread(new Tock(ticktock));
		
		tick.start();
		tock.start();
		try {
		tick.join();
		tock.join();
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
	}
	
}
