package com.sapient.week1.day4;

import java.util.ArrayList;

public class EmployeeDataStore {
	ArrayList<Employee> employeeList;
	
	public EmployeeDataStore() {
		employeeList = new ArrayList<>();
	}
	
	public void addEmployee(Employee emp)
	{
		employeeList.add(emp);
	}
	
	public void displayEmployee(ArrayList<String> rowList)
	{
		for(Employee emp : employeeList)
		{
			for(String str: rowList)
			{
				str = str.trim();
				if(str.equals("*"))
				{
					emp.display();
					continue;
				}
				else if(str.equalsIgnoreCase("name"))
				{
					System.out.print(emp.getName()+",");
				}
				else if(str.equalsIgnoreCase("age"))
				{
					System.out.print(emp.getAge()+",");
				}
				else if(str.equalsIgnoreCase("address"))
				{
					System.out.print(emp.getAddress()+",");
				}
				else if(str.equalsIgnoreCase("salary"))
				{
					System.out.print(emp.getSalary()+",");
				}
			}
			System.out.println();
		}
	}
	
}
