package com.sapient.week1.day4;

public class Tick implements Runnable {

	TickTock ticktock;
	
	public Tick(TickTock ticktock) {
		this.ticktock = ticktock;
	}
	
	@Override
	public void run() {
		for(int i=0;i<10;i++)
		{
			ticktock.tick();
		}
	}
	
	
}
