package com.sapient.week1.day4;

import java.util.Scanner;

public class SQLMain {
	public static void main(String[] args) {
		EmployeeDataStore store = new EmployeeDataStore();
		
		store.addEmployee(new Employee("Ishbir", 21, "Delhi", 100000));
		store.addEmployee(new Employee("Karan", 22, "Delhi", 10000));
		store.addEmployee(new Employee("Samson", 21, "Gurgaon", 100000));
		
		SQLParser sql = new SQLParser(store);
		System.out.println("Enter the query");
		Scanner sc = new Scanner(System.in);
		String query = sc.nextLine();
		
		sql.Parse(query);
	}
}
