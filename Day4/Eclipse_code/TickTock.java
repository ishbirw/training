package com.sapient.week1.day4;


public class TickTock {
	
	public synchronized void tick()
	{
		notify();
		System.out.print("Tick -");
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void tock()
	{
		notify();
		System.out.println("Tock");
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
