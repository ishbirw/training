package com.sapient.week1.day4;

public class Tock implements Runnable{

	TickTock ticktock;
	public Tock(TickTock ticktock) {
		this.ticktock = ticktock;
	}
	
	@Override
	public void run() {
		for(int i=0;i<10;i++)
		{
			if(i==0)
			{
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}				
			}
			ticktock.tock();
		}
	}

}
