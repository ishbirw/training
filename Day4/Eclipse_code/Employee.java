package com.sapient.week1.day4;

public class Employee {
	private String name;
	private int age;
	private String address;
	private int salary;
	
	public Employee(String name,int age, String addr,int salary) {
		this.name = name;
		this.age =age;
		this.address = addr;
		this.salary = salary;
	}
	
	public void display()
	{
		System.out.print(name +","+ age +","+ address +","+ salary);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	
}
