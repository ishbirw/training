package com.sapient.week1.day4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class FileRowSum {
	
	public static void main(String[] args) {
		try
		{
			BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\ishwalia\\Desktop\\Workspace\\project-week1\\src\\com\\sapient\\week1\\day4\\NumberData.csv"));
			BufferedWriter bw = new BufferedWriter(new FileWriter("C:\\Users\\ishwalia\\Desktop\\Workspace\\project-week1\\src\\com\\sapient\\week1\\day4\\NumberDataResult.csv"));
			
			String line;
			
			while((line = br.readLine())!= null)
			{
				String tmp[] = line.split(",");
				int sum =0;
				sum = Arrays.stream(tmp).mapToInt(num->Integer.parseInt(num.trim())).sum();
				line += " = " + sum;
				bw.write(line);
				bw.newLine();
			}
			br.close();
			bw.close();
			System.out.println("Done");
		}
		catch(IOException e){
			e.printStackTrace();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		catch(Throwable e) {
			e.printStackTrace();
		}
	}
	
}
