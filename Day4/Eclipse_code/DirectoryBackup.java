package com.sapient.week1.day4;
import java.io.*;
import java.nio.file.Files;

public class DirectoryBackup {
	
	public void copyFiles(File source, File target) throws IOException
	{
		File[] fileList = source.listFiles();
		
		for(File tmp_f : fileList)
		{
			System.out.println("Filename ="+tmp_f.getName());
			Files.copy(tmp_f.toPath(),new File(target.getAbsolutePath()+"\\"+tmp_f.getName()).toPath());
			
			if(tmp_f.isDirectory())
			{
				copyFiles(tmp_f, new File(target.getAbsolutePath()+"\\"+tmp_f.getName()));
			}
		}
	}

	public static void main(String[] args) {
		
		File source = new File("C:\\Users\\ishwalia\\Downloads\\Source");
		File target = new File("C:\\Users\\ishwalia\\Downloads\\Target");
		DirectoryBackup db = new DirectoryBackup();
		System.out.println("Starting Backup");
		try 
		{
			db.copyFiles(source, target);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Backup Done");
		
	}
}
