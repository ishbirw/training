package com.sapient.week1.day5;

import java.util.List;

public class MainSQL {
	
	public static void main(String[] args) {
		
		CricketDAO cricket = new CricketDAO();
		List<PlayerBean> plb;
		try {
			cricket.updatePlayer(new PlayerBean(10,"Rajat","Kumar",30));
			plb = cricket.getPlayers();
			for(PlayerBean tmp: plb)
			{
				System.out.println(tmp);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		
	}
}
