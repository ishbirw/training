package com.sapient.week1.day5;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CricketDAO {
	
	public List<PlayerBean> getPlayers() throws Exception 
	{
		Connection co = DBConnect.getConnection();
		PreparedStatement statement = co.prepareStatement("select pid,firstname,lastname,jerseyno from player");
		ResultSet res = statement.executeQuery();
		List<PlayerBean> playerList = new ArrayList<>();
		while(res.next())
		{
			playerList.add(new PlayerBean(res.getInt(1),res.getString(2),res.getString(3),res.getInt(4)));
		}	
		return playerList;
	}
	
	public List<PlayerBean> getPlayerFromID(int id) throws Exception 
	{
		Connection co = DBConnect.getConnection();
		PreparedStatement statement = co.prepareStatement("select pid,firstname,lastname,jerseyno from player where pid=?");
		statement.setInt(1, id);
		ResultSet res = statement.executeQuery();
		List<PlayerBean> playerList = new ArrayList<>();
		while(res.next())
		{
			playerList.add(new PlayerBean(res.getInt(1),res.getString(2),res.getString(3),res.getInt(4)));
		}	
		co = null;
		return playerList;
	}
	
	
	public int insertPlayer(PlayerBean player) throws Exception
	{
		Connection co = DBConnect.getConnection();
		PreparedStatement statement = co.prepareStatement("insert into player values(?,?,?,?)");
		statement.setInt(1, player.getPid());
		statement.setString(2, player.getFname());
		statement.setString(3, player.getLname());
		statement.setInt(4, player.getJerseyno());
		
		return statement.executeUpdate();		
	}
	
	
	public int deletePlayer(int id) throws Exception 
	{
		Connection co = DBConnect.getConnection();
		PreparedStatement statement = co.prepareStatement("delete from player where pid=?");
		statement.setInt(1, id);
		
		return statement.executeUpdate();
	}
	
	public int updatePlayer(PlayerBean player) throws Exception
	{
		Connection co = DBConnect.getConnection();
		PreparedStatement statement = co.prepareStatement("update player set firstname=?,lastname=?,jerseyno=? where pid=?");
		statement.setString(1, player.getFname());
		statement.setString(2, player.getLname());
		statement.setInt(3, player.getJerseyno());
		statement.setInt(4, player.getPid());
		
		return statement.executeUpdate();		
	}
	
	public void join() throws Exception
	{
		Connection co = DBConnect.getConnection();
		PreparedStatement statement = co.prepareStatement("select pid,firstname,lastname,jerseyno,matchid,runs from player,match where player.pid=match.pid");
		ResultSet res = statement.executeQuery();
		List<PlayerBean> playerList = new ArrayList<>();
		while(res.next())
		{
			playerList.add(new PlayerBean(res.getInt(1),res.getString(2),res.getString(3),res.getInt(4)));
		}	
		co = null;
		return;
//		return playerList;
	}
	
	
	
	
	
	
	
}
