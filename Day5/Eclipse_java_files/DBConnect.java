package com.sapient.week1.day5;

import java.sql.*;

public class DBConnect {
	public static Connection co;
	
	public static Connection getConnection() throws Exception
	{
		if(co==null)
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");  
			co=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","hr","hr"); 
		}
		return co;
	}
}
