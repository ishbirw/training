package com.sapient.week1.day5;
import java.io.*;
import java.nio.file.Files;

public class Sensible {
	public static void main(String[] args) {
		long max = new Long(4294967296l);
		long min = new Long(100000l);
		File file ;
		FileWriter f;
		BufferedWriter bf; 
		try {
			file = new File("C:\\Users\\ishwalia\\Desktop\\Workspace\\project-week1\\src\\com\\sapient\\week1\\day5\\Test.csv");
			f = new FileWriter(file);
			bf = new BufferedWriter(f);
			
			double i = 0;
			do {
				++i;
				try {
					bf.write(Double.toString(i) + "," + Double.toString(++i) + "\r\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println(Files.size(file.toPath()));
			}while(Files.size(file.toPath()) < min );
			bf.close();
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("File init");
		String line = "";
		String tmp = "";
		BufferedReader br;
		try {
			do
			{
				file = new File("C:\\Users\\ishwalia\\Desktop\\Workspace\\project-week1\\src\\com\\sapient\\week1\\day5\\Test.csv");
				if(line.length() < 704659450)
				{
					line = "";
					br = new BufferedReader(new FileReader(file));
					while((tmp = br.readLine())!=null)
					{
						line+=tmp;
					}
					if(line.length()>704659456)
					{
						line = line.substring(0,704659456);
					}
					bf = new BufferedWriter(new FileWriter(file));
				}
				else
				{
					bf = new BufferedWriter(new FileWriter(file,true));
				}
				bf.write(line);
				bf.write("\r\n"+line);
				System.out.println(Files.size(file.toPath()));
			}while(Files.size(file.toPath()) < max);
			
			bf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		System.out.println("Done");
		
	}
}
