package com.sapient.week1.day5;

public class PlayerBean {
	private int pid;
	private String fname;
	private String lname;
	private int jerseyno;
	
	public PlayerBean() {
		super();
	}
	
	public PlayerBean(int pid, String fname, String lname, int jerseyno) {
		super();
		this.pid = pid;
		this.fname = fname;
		this.lname = lname;
		this.jerseyno = jerseyno;
	}
	@Override
	public String toString() {
		return "PlayerBean [pid=" + pid + ", fname=" + fname + ", lname=" + lname + ", jerseyno=" + jerseyno + "]";
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public int getJerseyno() {
		return jerseyno;
	}
	public void setJerseyno(int jerseyno) {
		this.jerseyno = jerseyno;
	}
	
	
}
